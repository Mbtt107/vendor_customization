# Busybox
PRODUCT_PACKAGES += \
    busybox

# Magisk
ifeq ($(WITH_ROOT_METHOD), magisk)
PRODUCT_PACKAGES += \
    MagiskManager

PRODUCT_COPY_FILES += \
    vendor/customization/magisk/addon.d/Magisk.zip:system/addon.d/magisk.zip
endif

# Weather Provider
PRODUCT_PACKAGES += \
    YahooWeatherProvider

# Viper4Android
PRODUCT_PACKAGES += \
	Viper4Android

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/app/Viper4Android/lib/arm/libV4AJniUtils.so:system/app/Viper4Android/lib/arm/libV4AJniUtils.so \
    $(LOCAL_PATH)/lib/libdlbdapstorage.so:system/lib/libdlbdapstorage.so \
    $(LOCAL_PATH)/lib/libstagefright_soft_ddpdec.so:system/lib/libstagefright_soft_ddpdec.so \
    $(LOCAL_PATH)/lib/libV4AJniUtils.so:system/lib/libV4AJniUtils.so \
    $(LOCAL_PATH)/lib/soundfx/libeffectproxy.so:system/lib/soundfx/libeffectproxy.so \
    $(LOCAL_PATH)/lib/soundfx/libswdax.so:system/lib/soundfx/libswdax.so \
    $(LOCAL_PATH)/lib/soundfx/libv4a_fx_ics.so:system/lib/soundfx/libv4a_fx_ics.so \
    $(LOCAL_PATH)/su.d/audmodlib.sh:system/su.d/audmodlib.sh \
    $(LOCAL_PATH)/su.d/soundserver:system/su.d/soundserver \
    $(LOCAL_PATH)/vendor/etc/audio_effects.conf:system/vendor/etc/audio_effects.conf
